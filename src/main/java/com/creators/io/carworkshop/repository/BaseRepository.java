package com.creators.io.carworkshop.repository;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BaseRepository<T> {
    final Set<T> collection = new HashSet();

    public void create(T dto) {
        collection.add(dto);
    }

    public void delete(T dto) {
        collection.remove(dto);
    }

    public boolean contains(Predicate<T> predicate) {
        return collection.stream()
                .anyMatch(predicate);
    }

    public Set<T> list(Predicate<T> predicate) {
        return collection.stream()
                .filter(predicate)
                .collect(Collectors.toSet());
    }

    public void update(Predicate<T> filter, Consumer<T> update) {
         collection.stream()
                .filter(filter)
                .forEach(update);
    }
}

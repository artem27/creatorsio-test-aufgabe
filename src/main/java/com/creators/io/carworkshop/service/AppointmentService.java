package com.creators.io.carworkshop.service;

import com.creators.io.carworkshop.dto.Appointment;


public interface AppointmentService {

    void create(Appointment dto);

    void delete(Appointment dto);

    void updateTime(Appointment dto);

}

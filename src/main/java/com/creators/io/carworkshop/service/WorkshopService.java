package com.creators.io.carworkshop.service;

import com.creators.io.carworkshop.dto.Workshop;

import java.util.Collection;

public interface WorkshopService {

    void create(Workshop dto);

    void delete(Workshop dto);

    Collection<Workshop> list(String city);
}

package com.creators.io.carworkshop.service;

import com.creators.io.carworkshop.dto.User;

public interface UserService {

    void create(User dto);

    void delete(User dto);
}

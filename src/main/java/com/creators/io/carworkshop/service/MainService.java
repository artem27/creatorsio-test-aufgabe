package com.creators.io.carworkshop.service;

import com.creators.io.carworkshop.dto.Appointment;
import com.creators.io.carworkshop.dto.User;
import com.creators.io.carworkshop.dto.Workshop;
import com.creators.io.carworkshop.repository.BaseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;

@Service
@RequiredArgsConstructor
public class MainService implements UserService, WorkshopService, AppointmentService {
    private final BaseRepository<User> userRepository;
    private final BaseRepository<Workshop> workshopRepository;
    private final BaseRepository<Appointment> appointmentRepository;

    @Override
    public void create(User dto) {
        this.userRepository.create(dto);
    }

    @Override
    public void delete(User dto) {
        if (appointmentRepository.contains(appointment -> appointment.getUsername().equals(dto.getName()))) {
            throw new IllegalStateException(String.format("This is impossible because appointment with username %s is exist", dto.getName()));
        }
        this.userRepository.delete(dto);
    }

    @Override
    public void create(Workshop dto) {
        workshopRepository.create(dto);
    }

    public void delete(Workshop dto) {
        if (workshopRepository.contains(workshop -> workshop.getCompanyName().equals(dto.getCompanyName()))) {
            throw new IllegalStateException(String.format("This is impossible because appointment with companyName %s is exist", dto.getCompanyName()));
        }
        workshopRepository.delete(dto);
    }

    public Collection<Workshop> list(String city) {
        return workshopRepository.list(workshop -> city.equals(workshop.getCity()));
    }

    @Override
    public void create(Appointment dto) {
        if (!userRepository.contains(user -> user.getName().equals(dto.getUsername())) || !workshopRepository.contains(workshop -> workshop.getCompanyName().equals(dto.getCompanyName()))) {
            throw new IllegalArgumentException(String.format("This is impossible to create appointment with username %s and company name %s is not exist",
                    dto.getUsername(), dto.getCompanyName()));
        }
        appointmentRepository.create(dto);
    }

    @Override
    public void delete(Appointment dto) {
        appointmentRepository.delete(dto);
    }

    public void updateTime(Appointment dto) {
        appointmentRepository
                .update((appointment -> dto.getCompanyName().equals(appointment.getCompanyName()) && dto.getUsername().equals(appointment.getUsername())),
                        appointment -> appointment.setDateTime(LocalDateTime.now()));
    }

}

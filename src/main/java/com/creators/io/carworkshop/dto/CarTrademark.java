package com.creators.io.carworkshop.dto;

import java.io.Serializable;

public enum CarTrademark implements Serializable {
    BMW, VW
}

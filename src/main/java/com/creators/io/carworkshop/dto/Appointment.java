package com.creators.io.carworkshop.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class Appointment implements Serializable {
    @NotEmpty
    private String username;
    private CarTrademark usersTrademark;
    @NotEmpty
    private String companyName;
    @JsonIgnore
    private LocalDateTime dateTime = LocalDateTime.now();
}

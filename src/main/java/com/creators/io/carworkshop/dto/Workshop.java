package com.creators.io.carworkshop.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Data
public class Workshop implements Serializable {
    @EqualsAndHashCode.Include
    @NotEmpty
    private String companyName;
    private CarTrademark trademark;
    private String city;
    private String postalCode;
    private String country;
}

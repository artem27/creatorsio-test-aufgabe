package com.creators.io.carworkshop.controller;

import com.creators.io.carworkshop.dto.User;
import com.creators.io.carworkshop.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService service;

    @Operation(description = "Create a user")
    @PostMapping
    public void create(@Valid @RequestBody User dto) {
        service.create(dto);
    }

    @Operation(description = "Delete an existent user")
    @DeleteMapping
    public void delete(@Valid @RequestBody User dto) {
        service.delete(dto);
    }

}

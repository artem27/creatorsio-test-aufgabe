package com.creators.io.carworkshop.controller;

import com.creators.io.carworkshop.dto.Appointment;
import com.creators.io.carworkshop.service.AppointmentService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/appointment")
@RequiredArgsConstructor
public class AppointmentController {

    private final AppointmentService service;

    @Operation(description = "Create an appointment between a user and a car workshop")
    @PostMapping
    public void create(@Valid @RequestBody Appointment dto) {
        service.create(dto);
    }

    @Operation(description = "Change the date and time of an existent appointment")
    @PutMapping("/time")
    public void updateTime(@Valid @RequestBody Appointment dto) {
        service.updateTime(dto);
    }

    @Operation(description = "Delete an existent appointment")
    @DeleteMapping
    public void delete(@Valid @RequestBody Appointment dto) {
        service.delete(dto);
    }

}

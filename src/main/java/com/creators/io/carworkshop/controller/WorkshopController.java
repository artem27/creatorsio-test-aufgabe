package com.creators.io.carworkshop.controller;

import com.creators.io.carworkshop.dto.Workshop;
import com.creators.io.carworkshop.service.WorkshopService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.Collection;

@RestController
@RequestMapping("workshop")
@RequiredArgsConstructor
public class WorkshopController {

    private final WorkshopService service;

    @Operation(description = "Create a car workshop")
    @PostMapping
    public void create(@Valid @RequestBody Workshop dto) {
        service.create(dto);
    }

    @Operation(description = "Delete an existent car workshop")
    @DeleteMapping
    public void delete(@Valid @RequestBody Workshop dto) {
        service.delete(dto);
    }

    @Operation(description = "Search for all car workshop in a specific city")
    @GetMapping
    public Collection<Workshop> list(@Valid @RequestParam("city") @NotEmpty String city) {
        return service.list(city);
    }
}
